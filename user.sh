#!/bin/sh
export SEARCH="${SEARCH_FROM_ENV:-google}"
envsubst '${SEARCH}' < /etc/nginx/nginx.conf > /etc/nginx/nginx.conf
nginx -g 'daemon off;'