-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               10.7.3-MariaDB-1:10.7.3+maria~focal - mariadb.org binary distribution
-- Операционная система:         debian-linux-gnu
-- HeidiSQL Версия:              11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Дамп структуры для таблица exchange.attributes
CREATE TABLE IF NOT EXISTS `attributes` (
  `product` varchar(68) NOT NULL,
  `attribute` varchar(68) NOT NULL,
  `value` varchar(68) NOT NULL,
  `attribute_name` varchar(100) NOT NULL DEFAULT '',
  `value_name` varchar(100) NOT NULL DEFAULT '',
  UNIQUE KEY `unq_idx` (`product`,`attribute`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='    public $product;\r\n    public $attribute;\r\n    public $value;\r\n    public $attribute_name;\r\n    public $value_name;';

-- Экспортируемые данные не выделены.

-- Дамп структуры для таблица exchange.attributes_joined
CREATE TABLE IF NOT EXISTS `attributes_joined` (
  `product` varchar(68) NOT NULL,
  `attribute` varchar(68) NOT NULL,
  `value` varchar(68) NOT NULL,
  `attribute_name` varchar(100) NOT NULL DEFAULT '',
  `value_name` varchar(100) NOT NULL DEFAULT '',
  UNIQUE KEY `unq_idx` (`product`,`attribute`) USING BTREE,
  KEY `idx_key` (`attribute_name`,`value_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='    public $product;\r\n    public $attribute;\r\n    public $value;\r\n    public $attribute_name;\r\n    public $value_name;';

-- Экспортируемые данные не выделены.

-- Дамп структуры для процедура exchange.attributes_joined_proc
DELIMITER //
CREATE PROCEDURE `attributes_joined_proc`()
BEGIN
INSERT INTO attributes_joined
(`product`,`attribute`,`value`,`attribute_name`,`value_name`)

SELECT a.product, a.attribute, a.value,
		 pv.name AS attribute_name, pv.value_name AS value_name
FROM attributes AS a

JOIN props_values AS pv
ON pv.guid = a.attribute AND pv.value_guid = a.value;

END//
DELIMITER ;

-- Дамп структуры для таблица exchange.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `guid` varchar(68) NOT NULL,
  `name` varchar(128) NOT NULL,
  `cef_name` varchar(128) NOT NULL,
  `parent` varchar(68) NOT NULL,
  `depth` int(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`guid`),
  KEY `idx` (`parent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='    public $guid;\r\n    public $name;\r\n    public $cef_name;\r\n    public $parent;\r\n    public $depth;\r\n';

-- Экспортируемые данные не выделены.

-- Дамп структуры для таблица exchange.contragents
CREATE TABLE IF NOT EXISTS `contragents` (
  `guid` varchar(68) NOT NULL,
  `name` varchar(128) NOT NULL,
  `official` varchar(255) NOT NULL,
  `inn` bigint(20) unsigned NOT NULL DEFAULT 0,
  `kpp` bigint(20) unsigned NOT NULL DEFAULT 0,
  `okpo` int(11) unsigned NOT NULL DEFAULT 0,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`guid`),
  KEY `idx_key` (`inn`,`kpp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Экспортируемые данные не выделены.

-- Дамп структуры для таблица exchange.images
CREATE TABLE IF NOT EXISTS `images` (
  `product` varchar(68) NOT NULL,
  `file` varchar(355) NOT NULL,
  `filename` varchar(355) NOT NULL,
  `url` varchar(355) NOT NULL,
  `visible` int(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  UNIQUE KEY `unq_idx` (`product`,`filename`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Экспортируемые данные не выделены.

-- Дамп структуры для таблица exchange.managers
CREATE TABLE IF NOT EXISTS `managers` (
  `guid` varchar(68) NOT NULL,
  `name` varchar(128) NOT NULL,
  `subdivision` varchar(68) NOT NULL,
  `email` varchar(68) NOT NULL,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Экспортируемые данные не выделены.

-- Дамп структуры для таблица exchange.prices
CREATE TABLE IF NOT EXISTS `prices` (
  `guid` varchar(68) NOT NULL,
  `product` varchar(68) NOT NULL,
  `price` decimal(20,2) NOT NULL DEFAULT 0.00,
  `view` varchar(68) NOT NULL DEFAULT '',
  `nds` int(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`guid`,`product`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Экспортируемые данные не выделены.

-- Дамп структуры для таблица exchange.price_types
CREATE TABLE IF NOT EXISTS `price_types` (
  `guid` varchar(68) NOT NULL,
  `name` varchar(128) NOT NULL,
  `contragent` varchar(68) DEFAULT NULL,
  `partner` varchar(68) NOT NULL DEFAULT '',
  `manager` varchar(68) DEFAULT NULL,
  `nds` tinyint(1) unsigned NOT NULL DEFAULT 1,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `active` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `margin` decimal(6,2) NOT NULL DEFAULT 0.00,
  `discount` decimal(6,2) NOT NULL DEFAULT 0.00,
  `status` varchar(50) NOT NULL DEFAULT '',
  `date` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`guid`),
  KEY `idx` (`name`,`contragent`,`manager`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='`guid` ,`name`,`contragent`,`partner` ,`manager` ,`nds` ,`deleted` ,`active` `margin` `discount`,`status` ,`date` ';

-- Экспортируемые данные не выделены.

-- Дамп структуры для таблица exchange.products
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guid` varchar(68) NOT NULL,
  `code` varchar(68) NOT NULL,
  `shortcode` varchar(68) NOT NULL,
  `article` varchar(128) DEFAULT '',
  `name` varchar(255) DEFAULT '',
  `description` text DEFAULT '',
  `manufacturer` varchar(128) DEFAULT NULL,
  `nds` decimal(5,2) DEFAULT 20.00,
  `category` varchar(68) DEFAULT NULL,
  `cef_name` varchar(255) DEFAULT NULL,
  `visible` int(1) DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unq_idx` (`guid`,`code`),
  KEY `idx` (`guid`,`code`,`shortcode`,`category`)
) ENGINE=InnoDB AUTO_INCREMENT=24137 DEFAULT CHARSET=utf8mb4;

-- Экспортируемые данные не выделены.

-- Дамп структуры для таблица exchange.products_joined
CREATE TABLE IF NOT EXISTS `products_joined` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guid` varchar(68) NOT NULL,
  `code` varchar(68) NOT NULL,
  `shortcode` varchar(68) NOT NULL,
  `article` varchar(128) DEFAULT '',
  `name` varchar(255) DEFAULT '',
  `cef_name` varchar(255) DEFAULT NULL,
  `description` text DEFAULT '',
  `manufacturer` varchar(128) DEFAULT NULL,
  `cat1_name` varchar(255) DEFAULT NULL,
  `cat1_cef_name` varchar(255) DEFAULT NULL,
  `cat1_guid` varchar(68) DEFAULT NULL,
  `cat1_image` varchar(255) DEFAULT NULL,
  `cat2_name` varchar(255) DEFAULT NULL,
  `cat2_cef_name` varchar(255) DEFAULT NULL,
  `cat2_guid` varchar(68) DEFAULT NULL,
  `cat2_image` varchar(255) DEFAULT NULL,
  `cat3_name` varchar(255) DEFAULT NULL,
  `cat3_cef_name` varchar(255) DEFAULT NULL,
  `cat3_guid` varchar(68) DEFAULT NULL,
  `cat3_image` varchar(255) DEFAULT NULL,
  `price` decimal(20,2) DEFAULT NULL,
  `visible` int(1) DEFAULT 1,
  `nds` decimal(5,2) DEFAULT 20.00,
  `price_view` varchar(58) DEFAULT NULL,
  `price_type` varchar(128) DEFAULT NULL,
  `attributes` text DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `unq_idx` (`guid`,`code`) USING BTREE,
  KEY `idx` (`guid`,`code`,`shortcode`) USING BTREE,
  KEY `category_cef_name_idx` (`cat1_cef_name`,`cat2_cef_name`,`cat3_cef_name`) USING BTREE,
  KEY `category_name_idx` (`cat1_name`,`cat2_name`,`cat3_name`)
) ENGINE=InnoDB AUTO_INCREMENT=2048 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Экспортируемые данные не выделены.

-- Дамп структуры для процедура exchange.products_joined_proc
DELIMITER //
CREATE PROCEDURE `products_joined_proc`()
BEGIN
START TRANSACTION;
TRUNCATE products_joined;

INSERT INTO products_joined
(`guid`,`code`,`shortcode`,`name`,`cef_name`, `description`, `manufacturer`,
`cat1_name`,`cat1_cef_name`,`cat1_guid`,
`cat2_name`,`cat2_cef_name`,`cat2_guid`,
`cat3_name`,`cat3_cef_name`,`cat3_guid`,
`price`, `price_type`, `price_view`,
`cat1_image`, `cat2_image`,`cat3_image`,
`attributes`
)
SELECT DISTINCT p.guid, p.code, p.shortcode, p.name, p.cef_name, p.description, p.manufacturer,
			cat1.name AS cat1_name, cat1.cef_name AS cat1_cef_name, cat1.guid AS cat1_guid,
			cat2.name AS cat2_name, cat2.cef_name AS cat2_cef_name, cat2.guid AS cat2_guid,
			cat3.name AS cat3_name, cat3.cef_name AS cat3_cef_name, cat3.guid AS cat3_guid,
			pr.price, pt.name AS price_type, pr.`view` AS price_view,
			(SELECT images.file FROM images WHERE images.product = p.guid LIMIT 1) AS cat1_image,
			(SELECT images.file FROM images WHERE images.product = p.guid LIMIT 1) AS cat2_image,
			(SELECT images.file FROM images WHERE images.product = p.guid LIMIT 1) AS cat3_image,
			(SELECT 
			#CONCAT('{',GROUP_CONCAT(CONCAT('"',aj.attribute_name, '":"', aj.value_name, '"')) ,'}')
			#CONCAT('[',GROUP_CONCAT(JSON_OBJECT('name', aj.attribute_name,'phone', aj.value_name)),']')
			#CONCAT('[',GROUP_CONCAT(JSON_OBJECT( aj.attribute_name, aj.value_name)),']')
			CONCAT('{',
GROUP_CONCAT(
CONCAT(
		'"',
		REPLACE(REPLACE(aj.attribute_name, '"', '\\\"'),'\n','\\\\n'),
		'":"',
		REPLACE(REPLACE(aj.value_name, '"', '\\\"'),'\n','\\\\n'),
		'"')
		),
		'}')
			FROM attributes_joined AS aj
			WHERE aj.product = p.guid) AS attributes
			
			
FROM products AS p

INNER JOIN attributes_joined AS a
ON a.product = p.guid AND a.attribute_name = 'Выгружать на сайт' AND a.value_name = 'Да'
INNER JOIN categories AS cat3
ON cat3.guid = p.category
INNER JOIN categories AS cat2
ON cat2.guid = cat3.parent
INNER JOIN categories AS cat1
ON cat1.guid = cat2.parent
INNER JOIN price_types AS pt
ON pt.name = 'РРЦ (цена включает НДС)'
INNER JOIN prices AS pr
ON pr.guid = pt.guid AND pr.product = p.guid;
COMMIT;
END//
DELIMITER ;

-- Дамп структуры для таблица exchange.properties
CREATE TABLE IF NOT EXISTS `properties` (
  `product` varchar(68) NOT NULL,
  `property` varchar(128) NOT NULL,
  `value` varchar(128) NOT NULL,
  `property_name` varchar(100) DEFAULT '',
  `value_name` varchar(100) DEFAULT '',
  UNIQUE KEY `unq_idx` (`product`,`property`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='    public $product;\r\n    public $attribute;\r\n    public $value;\r\n    public $attribute_name;\r\n    public $value_name;';

-- Экспортируемые данные не выделены.

-- Дамп структуры для таблица exchange.props_values
CREATE TABLE IF NOT EXISTS `props_values` (
  `guid` varchar(68) NOT NULL,
  `name` varchar(68) NOT NULL,
  `value_guid` varchar(68) NOT NULL,
  `value_name` varchar(255) NOT NULL,
  UNIQUE KEY `unq_idx` (`guid`,`value_guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Экспортируемые данные не выделены.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
